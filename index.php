<?php

    # Acquire bootstrap
    require('system/bootstrap.php');

    # Retrieve next stock item number
    $stockItemNumber = (int) $db->query('select coalesce(max(number), 0) + 1 as next from stocktake')->getField();

?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?= $config['title'] ?></title>
    <meta name="Description" content="MFS JHB - Stock Take Database">
    <meta name="Author" content="andrewb@mediafilmservice.com">
    <meta name="RESOURCE-TYPE" content="DOCUMENT">
    <meta name="AUTHOR" content="Andrew Burger">
    <meta name="COPYRIGHT" content="Copyright (c) 2012 by MFS">
    <meta name="ROBOTS" content="INDEX, FOLLOW">
    <meta name="REVISIT-AFTER" content="1 DAYS">
    <meta name="RATING" content="GENERAL">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
	<div align="center">
		<p>
			<img src="assets/images/Medialog.JPG" width="381" height="123"><br>
		</p>
		<p>
			<strong><font size="4" face="Verdana, Arial, Helvetica, sans-serif">Stock Take Data Base</font> </strong><br><br>
		</p>
	</div>
	<?php if ( key_exists('added', $_GET) ) { ?>
	<div style="width:440px; margin:10px auto; padding:10px; border:1px solid #2C7501; background-color:#CCFEAE; color:#1E5101;">
	    Successfully added stock item to the database! Add another?
	</div>
	<?php } ?>
	<form action="insert.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="submitDetails" value="true" />
		<table border="0" align="center" cellpadding="2">
			<tr>
				<td width="165" valign="top" nowrap><font color="#000000"><strong>Number:</strong></font></td>
				<td width="180" valign="top">
				    <input type="text" name="item_number" value="<?php echo $stockItemNumber ?>" />
			    </td>
			</tr>
			<tr>
				<td valign="top" nowrap><strong>Item Number:</strong></td>
				<td valign="top">
				    <input name="item_nr" type="text" id="item_nr" value="Enter Item Stock Code" />
			    </td>
			</tr>
			<tr>
				<td valign="top" nowrap><strong><font color="#000000" size="2"
						face="Verdana">Item Name:</font> </strong></td>
				<td valign="top"><font color="#000000"> <input name="item_name"
						type="text" id="item_name" value="Enter Item Name">
				</font></td>
			</tr>
			<tr>
				<td valign="top" nowrap><font color="#000000"><strong>Item Barcode:</strong>
				</font></td>
				<td valign="top"><font color="#000000"> <input name="barcode"
						type="text" id="barcode" value="Scan Barcode Label" />
				</font></td>
			</tr>
			<tr>
				<td valign="top" nowrap>
				    <strong><font color="#000000" size="2" face="Verdana">Item Photo:</font></strong>
			    </td>
				<td valign="top">
				    <input name="photo" type="file" id="photo" />
				</td>
			</tr>
			<tr>
				<td valign="top" nowrap>
				    <strong><font color="#000000" size="2" face="Verdana">Date:</font></strong>
			    </td>
				<td valign="top">
				    <input name="date" type="text" id="date" value="<?php echo date('Y-m-d') ?>" />
				    <em>(YYYY-MM-DD)</em>
				</td>
			</tr>
			<tr>
				<td colspan="2"><div align="center"><input name="Submit" type="submit" id="Submit" value="Submit" /></div></td>
			</tr>
		</table>
	</form>
	<p>&nbsp;</p>
	<p align="center">
		<strong><a href="mailto:andrewb@mediafilmservice.com"><font size="1"
				face="Verdana, Arial, Helvetica, sans-serif">Developed by Andrew
					Burger for MFS </font> </a> </strong>
	</p>
</body>
</html>
