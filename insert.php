<?php

    # Acquire system bootstrap
    require('system/bootstrap.php');

    # Build the insert record array
    $record = array
    (
        'number'  => $_POST['item_number'],
        'sku'     => $_POST['item_nr'],
        'name'    => $_POST['item_name'],
        'barcode' => $_POST['barcode'],
        'date'    => $_POST['date']
    );

    # Upload the photo if one has been attached
    if ( key_exists('photo', $_FILES) and $_FILES['photo']['size'] > 0 )
    {
        $file =& $_FILES['photo'];

        # Fetch the uploaded photo's information
        $allowed   = array('jpg', 'png', 'gif');
        $extension = pathInfo($file['name'], PATHINFO_EXTENSION);

        # Ensure the file extension/type is permitted
        if ( !in_array($extension, $allowed) )
            die(sprintf('Photo image type not allowed (%s)', $extension));

        # Retrieve the proper image type
        $info = getImageSize($file['tmp_name']);

        # Determine extension
        switch( $info[2] )
        {
            default: $info['extension'] = false; break;

            case IMAGETYPE_JPEG:
            case IMAGETYPE_JPEG2000:
                $info['extension'] = 'jpg';
            break;

            case IMAGETYPE_GIF: $info['extension'] = 'gif'; break;
            case IMAGETYPE_PNG: $info['extension'] = 'png'; break;
        }

        # If a false extension has been returned, then the image is either invalid or
        # not an image... i.e. a hacker uploading a .exe renamed to a .jpg file for execution
        if ( $info['extension'] === false ) die('Invalid photo type uploaded!');

        # Generate a unique name for the photo using the current micro timestamp
        $imageName = md5(microtime(true)) .'.'. $info['extension'];
        $imagePath = trim($config['uploads'], '/') .'/'. $imageName;

        # Attempt to move the image to the uploads directory and if succeeded then append the
        # name to the database record for insertion
        if ( move_uploaded_file($file['tmp_name'], $imagePath) )
            $record['photo'] = $imageName;
    }

    # Ensure all data has been parsed
    $db->insert('stocktake', $record);

    # Redirect the user back to the form
    header('Location: index.php?added');
    die;

?>