<?php

    # Acquire bootstrap
    require('system/bootstrap.php');

    # Retrieve the list of stock items
    $where = key_exists('q', $_GET)
        ? "where sku like '%{$_GET['q']}%' or name like '%{$_GET['q']}%' or barcode like '%{$_GET['q']}%'"
        : null;

    $stockItems = $db->query("select * from stocktake {$where} order by id desc");

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title><?= $config['title'] ?></title>
        <link rel="stylesheet" type="text/css" href="assets/layout.css" />
    </head> 
    <body>

        <div class="wrapper">
            <div style="margin-bottom:15px; text-align:center;">
                <img src="assets/images/Medialog.JPG" width="381" height="123" />
            </div>
            <div class="body">
                <h3 style="float:left; margin-top:0;">Stock Tabel</h3>
                <div style="float:right; margin-bottom:10px;">
                    <form method="get" action="stock.php">
                        <input type="text" name="q" value="<?= @$_GET['q'] ?>" class="search" />
                        <input type="submit" value="Search" />
                    </form>
                </div>
                <div class="clear"></div>
                <table width="100%" cellpadding="0" cellspacing="0" class="listview">
                    <tr>
                        <th width="30" style="text-align:center">UID</th>
                        <th width="50" style="text-align:center">Number</th>
                        <th width="135">SKU</th>
                        <th>Name</th>
                        <th width="120">Barcode</th>
                        <th width="75">Date</th>
                    </tr>
                    <?php while( $item = $stockItems->fetch() ) { ?>
                    <tr>
                        <td align="center"><?= $item->id ?></td>
                        <td align="center"><?= $item->number ?></td>
                        <td><?= $item->sku ?></td>
                        <td><?= $item->name ?></td>
                        <td><?= $item->barcode ?></td>
                        <td><?= $item->date ?></td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>

    </body>
</html>