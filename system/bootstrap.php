<?php
/**
 * Basic Bootstrap
 *
 * This file initiates the required dependencies and integrates the
 * class auto loader for libraries.
 */

    # Acquire system configuration
    require('config.php');

    # Initiate constants and defaults
    define('SYSPATH', str_replace('\\', '/', dirName(__FILE__)));
    define('LIBPATH', SYSPATH .'/library');

    # Initiate a autoloader
    spl_autoload_register('SystemAutoLoader');

    /**
     * System Auto Loader Function
     *
     * Automatically attempts to load libraries and other system files on the fly
     */
    function SystemAutoLoader( $className )
    {
        $_className = strToLower($className);
        $_search    = array(LIBPATH .'/class.'. $_className .'.php', LIBPATH .'/driver.'. $_className .'.php');

        # Loop through each of the search paths defined aboved and if the desired
        # class file is found and the class exists then break away from the loop
        foreach( $_search as $path )
        {
            # Move onto the next loop iteration if the file doesn't exist
            if ( !file_exists($path) )
                continue;

            # Include the file and check to see if the class exists
            require($path);

            # Break away from the loop if the class exists
            if ( class_exists($className) ) break;
        }
    }

    # Automatically initiate/construct the database class if set in the configuration
    if ( $config['db']['autoinit'] === true and $config['db']['autoconnect'] === true )
        $db = new MySQL;

?>