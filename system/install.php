<?php
/**
 * Installation File
 *
 * Run this file in your browser to execute the system installation process. This
 * will attempt to create the neccessary database tables assuming the specified
 * user has been granted the correct access.
 *
 * Please note: the database.sql file is in fact a template and uses {references}
 * which this install file will load into memory and replace accordingly with the
 * specified database details.
 */

    # Acquire system bootstrap
    require('bootstrap.php');

    # Initiate an database instance if autoinit is disabled
    if ( $config['db']['autoinit'] !== true )
        $db = new MySQL;

    /*
    # First check to see if the tables already exists
    if ( $db->query("show tables like 'stocktake'")->numRows() > 0 )
        die('Error! Please drop and re-create the database before executing this script!');
    */

    # Load up the database.sql file into memory
    $sql = replaceTags(file_get_contents('database.sql'), $config);

    # Lambda function used to recursively loop through the configuration variable
    # and replace the $sql contents
    function replaceTags( $string, $config, $prefix = '' )
    {
        foreach( $config as $key => $value )
        {
            # Recursilvely replace tags
            if ( is_array($value) )
                $string = call_user_func_array(__FUNCTION__, array($string, $value, $key .'_'));

            # Attempt the replace
            else $string = str_replace('{'. ($prefix . $key) .'}', $value, $string);
        }

        return $string;
    }

    # Since we're not using mysqli or pdo, the default mysql extension doesn't support multiple
    # queries so we need to split the queries up by the delimeter and execute them individually
    $queries = array_filter(array_map('trim', explode(';', $sql)));

    # Now loop through each of the queries and run them
    foreach( $queries as $query ) $db->query($query);

    echo 'Installation process completed!';

?>