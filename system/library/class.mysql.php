<?php
/**
 * Basic MySQL Class
 *
 * Provides flexible database access to MySQL server
 */
class MySQL
{
    protected

        /**
         * Default Database Options
         */
        $options = array
        (
            'autoconnect' => true,
            'hostname'    => '127.0.0.1',
            'port'        => 3306,
            'username'    => 'root',
            'password'    => '',
            'database'    => 'test'
        ),

        /**
         * Connection Handle
         */
        $connection = false;

    /**
     * Class Constructor
     */
    function __construct( $options = array() )
    {
        # Automatically load up the global $config data if present
        global $config;

        if ( isset($config['db']) )
            $this->options = array_merge($this->options, $config['db']);

        # Overwrite the defaults and config with the $options parameter
        if ( !empty($options) )
            $this->options = array_merge($this->options, $options);

        # Automatically initiate database connection if 'autoconnect' option is enabled
        if ( $this->options['autoconnect'] === true )
            $this->connect();
    }

    /**
     * Initiate a connection to the database
     */
    public function connect( $options = array() )
    {
        # Overwrite the defaults and config with the $options parameter
        if ( !empty($options) )
            $this->options = array_merge($this->options, $options);

        # First attempt to initiate a connection to the database server
        $this->connection = @mysql_connect
        (
            $this->options['hostname'],
            $this->options['username'],
            $this->options['password']
        )
            or die(sprintf('%s: unable to establish connection to "%s" (%s)',
                __METHOD__, $this->options['hostname'], mysql_error()));

        # Attempt to select the specified database
        @mysql_select_db($this->options['database'])
            or die(sprintf('%s: unable to select database "%s" on "%s" (%s)',
                __METHOD__, $this->options['database'], $this->options['hostname'], mysql_error()));
    }

    /**
     * Queries the database
     */
    public function query( $query )
    {
        return new ResultSet(mysql_query($query, $this->connection));
    }

    /**
     * Sanitize/escape string
     */
    public function sanitize( $string )
    {
        return @mysql_real_escape_string($string, $this->connection);
    }

    /**
     * Insert php array into the specified table
     */
    public function insert( $table, array $record )
    {
        # Acquire fields and values and then build up the insert query
        $columns = array_keys($record);
        $values  = array_map(array($this, 'sanitize'), array_values($record));
        $query   = 'insert into `'. $table .'` (`'. implode('`, `', $columns) .'`) values (\''. implode("', '", $values) .'\')';

        return $this->query($query);
    }
}

/**
 * ResultSet Class
 */
class ResultSet
{
    protected $result = false;

    /**
     * Class Constructor
     */
    function __construct( $result )
    {
        $this->result =& $result;
    }

    /**
     * Return the number of rows returned by the result
     */
    public function numRows()
    {
        return @mysql_num_rows($this->result);
    }

    /**
     * Retrieve a an object form of the next result
     */
    public function fetch()
    {
        return @mysql_fetch_object($this->result);
    }

    /**
     * Retrieve a associative representation of the result
     */
    public function fetchRow()
    {
        return @mysql_fetch_assoc($this->result);
    }

    /**
     * Retrieve a single result field
     */
    public function getField()
    {
        return array_shift(mysql_fetch_array($this->result));
    }
}