use `{db_database}`;

drop table if exists stocktake;
create table stocktake
(
    id          int not null auto_increment,
    created     timestamp default current_timestamp,
    date        date null,
    number      int null,
    sku         varchar(100) null,
    name        varchar(100) null,
    barcode     varchar(100) null,
    photo       varchar(120) null,    

    primary key (id)
) engine = innodb default charset = utf8;