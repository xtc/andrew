<?php
/**
 * System Configuration
 */

    # General
    $config['title']   = 'MFS - Stock Take Database';
    $config['uploads'] = 'photos';

    # Database
    $config['db']['autoconnect'] = true;
    $config['db']['autoinit']    = true; // Automatically create and assign the $db variable (requires 'autoconnect' set to true)
    $config['db']['hostname']    = '127.0.0.1';
    $config['db']['port']        = 3306;
    $config['db']['username']    = 'root';
    $config['db']['password']    = '';
    $config['db']['database']    = 'andrew_stdb';

/**
 * Initiate PHP error reporting
 */

    error_reporting(E_ALL);
    ini_set('display_errors', true);

?>